# Modx docker setup instructions

* instructions below tested in Ubuntu 16.04  

## Initial setup

* switch to root user: ```sudo -i```  
* change ```server_name``` directive in ```nginx/site.conf```  
* change database password in ```docker-compose.yml```  

## Modx installation

1) Create directory to store DB data: ```mysql-data```  
2) Change current working directory to code: ```cd code```  
3) Download latest modx: ```wget http://modx.com/download/direct/modx-2.6.2-pl.zip```    
4) Unzip modx archive: ```unzip modx-2.6.2-pl.zip```    
5) Rename modx folder: ```mv modx-2.6.2-pl modx```  
6) Change modx directory owner: ```chown -R root:www-data modx```  
7) Change modx directory permissions: ```chmod -R 775 modx```  
8) Change directory: ```cd ..```  
9) Start docker containers: ```docker-compose up -d```    
10) Open ```/modx/setup``` page in your browser  
11) Use ```mysql``` as hostname of a database server  
12) Complete install  
